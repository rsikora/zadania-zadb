# -*- coding: utf-8 -*-
import Image
import os
import re
import zipfile
import shutil

from flask import Flask, render_template, send_from_directory
from flask import  session
from flask import request, redirect
from flask import flash, url_for

#################
#  Konfiguracja #
#################

DATABASE = 'googlr.db'
SECRET_KEY = '1234567890!@#$%^&*()'

USERS = {'admin':'admin'}

path = '/home/p1/dom/imagePage/static/upload'

#############
# Aplikacja #
#############

app = Flask(__name__)
app.config.from_object(__name__)

rozszerzenia = ['', 'jpeg', 'jpg', 'png', 'gif', 'bmp']


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        try:
            do_login(request.form['username'],
                     request.form['password'])
        except ValueError:
            flash(u"Błędne dane", 'error')
        else:
            flash(u'Zostałeś zalogowany', 'success')
            return redirect(url_for('main'))
    return render_template('login.html')


@app.route('/logout')
def logout():
    session.clear()
    flash(u'Zostałeś wylogowany')
    return redirect(url_for('main'))



@app.route('/images')
@app.route('/images/<path:catalog_name>/')
def images(catalog_name = ""):
    pliki = os.listdir(path)
    if len(catalog_name) > 0:
        try:
            pliki = os.listdir(path+"/"+catalog_name)
        except:
            return render_template('error.html',message="Nie ma takieogo katalogu")
    obrazki=[]
    for plik in pliki:
        splited = plik.split(".")
        splited.append('')
        if splited[1] in rozszerzenia:
            ###wyszukiwanie podkatalogow
            podfoldery = path.split('/')
            sciezka = ""
            print plik
            for podfolder in podfoldery:
                if (podfoldery.index(podfolder) >= podfoldery.index("upload")):
                    sciezka+=podfolder+"/"
            ###
            if len(catalog_name) > 0:
                catalog_name+="/"
            obrazki.append(sciezka+catalog_name+plik)
    return render_template('images.html',images=obrazki)

@app.route('/')
@app.route('/list/1/')
@app.route('/list/1/<path:catalog_name>/')
def list1(catalog_name = ""):
    obrazki = retImages(catalog_name)
    return render_template('list.html',images=obrazki, kol=1)

@app.route('/list/3/')
@app.route('/list/3/<path:catalog_name>/')
def list3(catalog_name = ""):
    obrazki = retImages(catalog_name)
    return render_template('list.html',images=obrazki, kol=3)

@app.route('/list/5/')
@app.route('/list/5/<path:catalog_name>/')
def list5(catalog_name = ""):
    obrazki = retImages(catalog_name)
    return render_template('list.html',images=obrazki, kol=5)

def retImages(catalog_name = ""):
    pliki = os.listdir(path)
    if len(catalog_name) > 0:
        try:
            pliki = os.listdir(path+"/"+catalog_name)
        except:
            return render_template('error.html',message="Nie ma takieogo katalogu")
    obrazki=[]
    for plik in pliki:
        splited = plik.split(".")
        splited.append('')
        if splited[1] in rozszerzenia:
            ###wyszukiwanie podkatalogow
            podfoldery = path.split('/')
            sciezka = ""
            for podfolder in podfoldery:
                if (podfoldery.index(podfolder) >= podfoldery.index("upload")):
                    sciezka+=podfolder+"/"
                    ###
            if (catalog_name[-1:] != '/' and len(catalog_name)>0):
                catalog_name+='/'
            obrazki.append(sciezka+catalog_name+plik)
    return obrazki


@app.route('/image/<path:sciezka>')
def index(sciezka):
    try:
        with open(path.replace('upload','')+sciezka): pass
    except:
        return render_template('error.html',message="Nie ma takiego pliku")
    katalogs = get_subdirectories(path)
    katalogs.append('')
    return render_template('image_view.html',image=sciezka, katalogi=katalogs)

@app.route('/upload')
def upload():
    return render_template('upload_image.html')

@app.route('/uploaded',methods=['GET', 'POST'])
def uploaded():
    success = False
    number = 0
    if request.method == 'POST' and 'image[]' in request.files:
        try:
            pliki = request.files.getlist('image[]')
            print pliki
            for plik in pliki:
                print plik.filename.split('.')[-1]
                if plik.filename.split('.')[-1] in rozszerzenia:
                    plik.save(path+'/'+plik.filename)
                    nazwapliku = plik.filename
                    scale_image(nazwapliku, 400)
                    scale_image(nazwapliku, 200)
                    scale_image(nazwapliku, 100)
                    success=True
                    number+=1
                elif plik.filename.split('.')[-1]=='zip':
                    print 'no to mamy zipek'
                    zfile=zipfile.ZipFile(file)
                    for nazwa in zfile.namelist():
                        nazwapliku = os.path.split(nazwa)[1]
                        if nazwapliku.split('.')[-1]in rozszerzenia:
                            fd = open(path+'/'+nazwapliku,"w")
                            fd.write(zfile.read(nazwa))
                            fd.close()
                            scale_image(nazwapliku, 400)
                            scale_image(nazwapliku, 200)
                            scale_image(nazwapliku, 100)
                            success=True
                            number+=1
        except:
            print "zle jest"
    return render_template('upload_complete.html',success=success,number=number)

@app.route('/zip/')
def zip_images():
    try:
        os.remove(path+'/Images.zip')
    except:
        pass
    zip = zipfile.ZipFile(path+'/Images.zip','w')
    pliki = os.listdir(path)
    for nazwapliku in pliki:
        print nazwapliku
        if nazwapliku.split('.')[-1] in rozszerzenia:
            zip.write(path+'/'+nazwapliku,arcname=nazwapliku)
    zip.close()
    return send_from_directory(path,'Images.zip',as_attachment=True)

@app.route('/zip/<path:image_name>/')
def zip_image(image_name):
    image_name = image_name.replace("upload/","")
    zipname_parts = image_name.split('.')[:-1]
    zipname=''
    for part in zipname_parts:
        zipname+=part+'.'
    zipname+='zip'
    zip = zipfile.ZipFile(path+'/'+zipname,'w')

    zip.write(path+'/'+image_name,arcname=image_name)
    zip.close()
    return send_from_directory(path,zipname,as_attachment=True)
    zip.delete()

@app.route('/delete/<path:image_name>/')
def delete_image(image_name):
    try:
        nazwa_pliku = image_name.split('.')[:-1]
        nazwa_pliku = nazwa_pliku[0].replace('upload','')
        print nazwa_pliku[0]
        os.remove(path+image_name.replace('upload',''))
        os.remove(path+nazwa_pliku+"100.min")
        os.remove(path+nazwa_pliku+"200.min")
        os.remove(path+nazwa_pliku+"400.min")
    except:
        pass
    return render_template('error.html',message="Skasowano obrazek")

@app.route('/add_catalog/<path:catalog_name>/', methods=['GET', 'POST'])
def add_catalog(catalog_name):
    if request.method == 'POST' and request.form['name_catalog'] != '':
        name = request.form['name_catalog']
        catalog_name = catalog_name.replace('upload','')
        catalog_name+="/"+name
        print path+catalog_name
        if not os.path.exists(path+catalog_name):
            os.makedirs(path+catalog_name)
            return render_template('error.html',message="Dodano katalog")
        else:
            return render_template('error.html',message="Katalog juz istnieje")
    else:
        return render_template('add_catalog.html',path=catalog_name)

@app.route('/move/<path:image_path>/', methods=['GET', 'POST'])
def move(image_path):
    if request.method == 'POST':
        zrodlo = path+image_path.replace('upload','')
        cel = path+'/'+request.form['lista']+'/'+zrodlo.split('/')[-1:][0]
        shutil.copyfile(zrodlo, cel)
        os.remove(zrodlo)

        zrodlo1 = zrodlo.split('.')[:1][0]+'100.min'
        cel1 = cel.split('.')[:1][0]+'100.min'
        shutil.copyfile(zrodlo1, cel1)
        os.remove(zrodlo1)

        zrodlo1 = zrodlo.split('.')[:1][0]+'200.min'
        cel1 = cel.split('.')[:1][0]+'200.min'
        shutil.copyfile(zrodlo1, cel1)
        os.remove(zrodlo1)

        zrodlo1 = zrodlo.split('.')[:1][0]+'400.min'
        cel1 = cel.split('.')[:1][0]+'400.min'
        shutil.copyfile(zrodlo1, cel1)
        os.remove(zrodlo1)

        return render_template('error.html',message="Przeniesiono")


################
# Autentykacja #
################

def do_login(usr, pwd):
    if not usr in app.config['USERS']:
        raise ValueError
    elif pwd != app.config['USERS'][usr]:
        raise ValueError
    else:
        session.clear()
        session['logged_in'] = True
        session['username'] = usr

@app.before_request
def update_username():
    if not session.get('username'):
        session['username'] = request.remote_addr

################
#  Skalowanie  #
################

def scale_image(filename, width1):
    image = Image.open(path+'/'+filename)

    width, height =image.size
    newim = image.resize((width1,width1*height/width), Image.ANTIALIAS)
    min = filename.split('.')[0]+"_min."+filename.split('.')[1]
    newim.save(path+'/'+min)
    os.rename(path+'/'+min, path+'/'+filename.split(".")[0]+str(width1)+".min")

################
# Uruchomienie #
################

def get_subdirectories(dir):
    pliki = os.listdir(dir)
    lista = []
    for plik in pliki:
        plik1 = plik
        plik1+='.'
        if plik1.split('.')[1:][0] == '':
            print plik
            sciezka = dir+'/'+plik
            sciezka = re.sub(r".*upload/", "", sciezka)
            lista.append(sciezka)
            lista+=(get_subdirectories(dir+'/'+plik))
    return lista

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=31001, debug=True)

