import memcache,random, time, timeit
from math import *

mc = memcache.Client(['194.29.175.241:11211','194.29.175.242:11211'])

found = 0
total = 0

def rozklad_czynniki(n):
    if n<=0:
        return 0
    i=2
    e=floor(sqrt(n))
    tab=[]
    while i<=e:
        if n%i==0:
            tab.append(i)
            n/=i
            e=floor(sqrt(n))
        else:
            i+=1
    if n>1:  tab.append(n)
    return tab

found = 0
total = 0

def compute_factorial(n):
    if n == 0:
        return 1
    global total, found
    total += 1
    value = mc.get(str(n))
    if value is None:
        time.sleep(0.001)
        value = rozklad_czynniki(n)
        mc.set(str(n), value)
    else:
        found += 1
    return value

def make_request():
    compute_factorial(random.randint(0, 1000))


print 'Czasy dla 500 wywolan:',
for i in range(1, 501):
    print '%.2fs, ratio=%.4f' % (timeit.timeit(make_request(), number=500), float(found)/total)
    print